<?php
/**
 * Bake Template for Controller action generation.
 *
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @package       Cake.Console.Templates.default.actions
 * @since         CakePHP(tm) v 1.3
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 */
?>

/**
 * <?php echo $admin ?>index method
 *
 * @return void
 */
	public function <?php echo $admin ?>index() {
		if ($this->request->is('get')) {
			$<?php echo $pluralName ?> = $this-><?php echo $currentModelName ?>->find('all');
			$this->_sendJson($<?php echo $pluralName ?>);
		}
	}

/**
 * <?php echo $admin ?>view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function <?php echo $admin ?>view($id = null) {
		if ($this->request->is('get')) {
			$options = array('conditions' => array('<?php echo $currentModelName; ?>.' . $this-><?php echo $currentModelName; ?>->primaryKey => $id));
			$<?php echo $singularName ?> = $this-><?php echo $currentModelName ?>->find('first', $options);
			$this->_sendJson($<?php echo $singularName ?>);
		}
	}

/**
 * <?php echo $admin ?>add method
 *
 * @return void
 */
	public function <?php echo $admin ?>add() {
		if ($this->request->is('post')) {
			$this-><?php echo $currentModelName; ?>->create();
			if ($this-><?php echo $currentModelName; ?>->save($this->request->data)) {
				$this->_sendJson(null, 'Created');
			} else {
				$this->_sendJson(null, 'Error');
			}
		}
	}

/**
 * <?php echo $admin ?>edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function <?php echo $admin; ?>edit($id = null) {
		if ($this->request->is(array('post', 'put'))) {
			$this-><?php echo $currentModelName; ?>->id = $id;
			if ($this-><?php echo $currentModelName; ?>->save($this->request->data)) {
				$this->_sendJson(null, 'Saved');
			} else {
				$this->_sendJson(null, 'Error');
			}
		}
	}

/**
 * <?php echo $admin ?>delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function <?php echo $admin; ?>delete($id = null) {
		if ($this->request->is(array('post', 'delete'))) {
			if ($this-><?php echo $currentModelName; ?>->delete($id)) {
				$message = 'Deleted';
			} else {
				$message = 'Error';
			}
		}
	}
