<?php

App::uses('Controller', 'Controller');

class RestFullAppController extends Controller
{
    protected function _sendJson($data = null, $message = null)
    {
        $this->autoRender = false;
        $this->response->disableCache();
        $this->response->modified('now');
        $this->response->checkNotModified($this->request);
        $this->response->type('application/json');
        $wrapper = $this->__createWrapper($data, $message);
        $this->response->body(json_encode($wrapper));
    }

    private function __createWrapper($data, $message)
    {
        return array(
            'message' => $message,
            'data'    => $data,
        );
    }
}
